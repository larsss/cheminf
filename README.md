# ChemInf Package: cheminformatics in R #

The ChemInf R package provides basic functionality for cheminformatics in R. 

## Features ##

* read, write and have fun with SDF files
* read and write SMILES
* draw compounds in R
* compound similarity search
* similarity assessment (tanimoto coefficient)
* integration with the **PubChem**, **CHEMBL** and **UniChem** databases

## User guide ##
Check out our [Wiki page](https://bitbucket.org/larsss/cheminf/wiki/Home) for basic examples, and the awesome [Cheminformatics analysis with ChemInf](https://bitbucket.org/larsss/cheminf/downloads/cheminf.pdf) vignette. 

## Installing the ChemInf package ##

The simplest approach of installing ChemInf is to use the R function install_bitbucket() from the devtools package:

```
#!r
install.packages("devtools")
devtools::install_bitbucket("larsss/cheminf", subdir = "cheminf/")
```


The ChemInf package builds on the [Bio3D](https://bitbucket.org/Grantlab/bio3d) package (version >=2.3-0) for biomolecular structure analysis. Installing Bio3D is therefore required prior to installing the ChemInf package. It also makes use of packages XML, xml2, and RCurl. These dependencies can be installed with the following command from the R console:

```
#!r
install.packages(c("bio3d", "XML", "xml2", "RCurl"))
```

The ChemInf package utilizes [Open Babel](http://openbabel.org/wiki/Main_Page) for various tasks. 

On Ubuntu, install babel with:

```
#!r
apt-get install openbabel
```

or on Fedora:

```
#!r
yum install openbabel
```

or on Mac OSX using the excellent [homebrew](https://brew.sh/) package manager:

```
#!r
brew install open-babel
```

### Note 
The **ChEMBL** interface is based on a previous release of the [chemblr](https://github.com/rajarshi/chemblr) package.