#include <Rcpp.h>
#include <iostream>
#include <fstream>
#include <string>
#include "utils.h"
#include "convert.h"
using namespace std;
using namespace Rcpp;

// [[Rcpp::export('.read_sdf')]]
List read_sdf(std::string filename) {

  // out is a List object
  Rcpp::List out;
  
  // to store for each molecule
  string name;
  string id;
  int natoms;
  int nbonds;

  double tmpx;
  double tmpy;
  double tmpz;
  
  vector<double> xyz;
  vector<double> x;
  vector<double> y;
  vector<double> z;
  vector<string> elety;
  
  vector<int> atom_a;
  vector<int> atom_b;
  vector<int> atom_type;

  vector<string> info_crap;
  vector<string> atom_crap;
  vector<string> bond_crap;
    
  // temp variables
  Rcpp::List tmp_list;
  //CharacterVector tmp_info;
  Rcpp::DataFrame tmp_info;
  Rcpp::DataFrame tmp_atom;
  Rcpp::DataFrame tmp_bond;

  // keep track of stuff
  int counter = 0;
  int nmol = 0;
  
  // for reading
  string line;
  ifstream myfile;

  // open file and iterate over each line
  myfile.open(filename.c_str());
  
  if (myfile.is_open())  {
    while ( getline (myfile,line) ) {

      counter+=1;
      
      // keep of track of number of molecules in SDF file
      if(line.substr(0,4)=="$$$$") {
	counter=0;
	nmol+=1;

	tmp_info = Rcpp::DataFrame::create(Rcpp::Named("atoms")=natoms,
					   Rcpp::Named("bonds")=nbonds,
					   Rcpp::Named("V1")=stringToInt(info_crap[2]),
					   Rcpp::Named("V2")=stringToInt(info_crap[3]),
					   Rcpp::Named("V3")=stringToInt(info_crap[4]),
					   Rcpp::Named("V4")=stringToInt(info_crap[5]),
					   Rcpp::Named("V5")=stringToInt(info_crap[6]),
					   Rcpp::Named("V6")=stringToInt(info_crap[7]),
					   Rcpp::Named("V7")=stringToInt(info_crap[8]),
					   Rcpp::Named("V8")=info_crap[9],
					   Rcpp::Named("V9")=info_crap[10],
					   Rcpp::Named("stringsAsFactors")=false);
					   
	
	tmp_atom = Rcpp::DataFrame::create(Rcpp::Named("x")=x,
					   Rcpp::Named("y")=y,
					   Rcpp::Named("z")=z,
					   Rcpp::Named("elety")=elety,
					   Rcpp::Named("V1")=stringToInt(atom_crap[2]),
					   Rcpp::Named("V2")=stringToInt(atom_crap[3]),
					   Rcpp::Named("V3")=stringToInt(atom_crap[4]),
					   Rcpp::Named("V4")=stringToInt(atom_crap[5]),
					   Rcpp::Named("V5")=stringToInt(atom_crap[6]),
					   Rcpp::Named("V6")=stringToInt(atom_crap[7]),
					   Rcpp::Named("V7")=stringToInt(atom_crap[8]),
					   Rcpp::Named("V8")=stringToInt(atom_crap[9]),
					   Rcpp::Named("V9")=stringToInt(atom_crap[10]),
					   Rcpp::Named("V10")=stringToInt(atom_crap[11]),
					   Rcpp::Named("V11")=stringToInt(atom_crap[12]),
					   Rcpp::Named("V12")=stringToInt(atom_crap[13]),
					   Rcpp::Named("stringsAsFactors")=false);
	
	tmp_bond = Rcpp::DataFrame::create(Rcpp::Named("a")=atom_a,
					   Rcpp::Named("b")=atom_b,
					   Rcpp::Named("type")=atom_type,
					   Rcpp::Named("V1")=stringToInt(bond_crap[0]),
					   Rcpp::Named("V2")=stringToInt(bond_crap[1]),
					   Rcpp::Named("V3")=stringToInt(bond_crap[2]),
					   Rcpp::Named("V4")=stringToInt(bond_crap[3]),
					   Rcpp::Named("stringsAsFactors")=false);

	tmp_list = Rcpp::List::create(Rcpp::Named("name")=name,
				      Rcpp::Named("id")=id,
				      Rcpp::Named("info")=tmp_info,
				      Rcpp::Named("atom")=tmp_atom,
				      Rcpp::Named("bond")=tmp_bond,
				      Rcpp::Named("xyz")=xyz);
	tmp_list.attr("class") = "sdf";

	// reset temp variables
	name="";
	id="";

	natoms=0;
	nbonds=0;
	x.clear();
	y.clear();
	z.clear();
	elety.clear();
	xyz.clear();
	atom_a.clear();
	atom_b.clear();
	atom_type.clear();

	info_crap.clear();
	bond_crap.clear();
	atom_crap.clear();
	
	
	out.push_back(tmp_list);
	continue;
      }
      
      if(counter==1) {
	name = trim(line);
      }

      if(counter==2) {
	id = trim(line);
      }

      // molinfo line
      if(counter==4) {
	natoms=stringToInt(line.substr(0,3));
	nbonds=stringToInt(line.substr(3,3));

	info_crap.push_back(trim(line.substr(0,3)));
	info_crap.push_back(trim(line.substr(3,3)));
	
	info_crap.push_back(trim(line.substr(6,3)));
	info_crap.push_back(trim(line.substr(9,3)));
	info_crap.push_back(trim(line.substr(12,3)));
	info_crap.push_back(trim(line.substr(15,3)));
	info_crap.push_back(trim(line.substr(18,3)));
	info_crap.push_back(trim(line.substr(21,3)));
	info_crap.push_back(trim(line.substr(24,3)));

	info_crap.push_back(trim(line.substr(27,6)));
	info_crap.push_back(trim(line.substr(33,6)));
			    
      }

      // read atom data
      if((counter > 4) && (counter <= 4+natoms)) {
	tmpx = stringToDouble(line.substr(0,10));
	tmpy = stringToDouble(line.substr(10,10));
	tmpz = stringToDouble(line.substr(20,10));
	
	x.push_back(tmpx);
	y.push_back(tmpy);
	z.push_back(tmpz);

	xyz.push_back(tmpx);
	xyz.push_back(tmpy);
	xyz.push_back(tmpz);
	elety.push_back(trim(line.substr(30,2)));
	
	atom_crap.push_back(trim(line.substr(33,3)));
	atom_crap.push_back(trim(line.substr(36,3)));
	atom_crap.push_back(trim(line.substr(39,3)));
	atom_crap.push_back(trim(line.substr(42,3)));
	atom_crap.push_back(trim(line.substr(45,3)));
	atom_crap.push_back(trim(line.substr(48,3)));
	atom_crap.push_back(trim(line.substr(51,3)));
	atom_crap.push_back(trim(line.substr(54,3)));
	atom_crap.push_back(trim(line.substr(57,3)));
	atom_crap.push_back(trim(line.substr(63,3)));
	atom_crap.push_back(trim(line.substr(66,3)));

      }

      // read bond data
      if((counter > 4+natoms) && (counter <= 4+natoms+nbonds)) {
	atom_a.push_back(stringToDouble(line.substr(0,3)));
	atom_b.push_back(stringToDouble(line.substr(4,3)));
	atom_type.push_back(stringToDouble(line.substr(7,3)));
	
	bond_crap.push_back(trim(line.substr(9,3)));
	bond_crap.push_back(trim(line.substr(12,3)));
	bond_crap.push_back(trim(line.substr(15,3)));
	bond_crap.push_back(trim(line.substr(18,3)));

      }

      
    }
  }
  
  out.attr("class") = "sdfs";
  return(out);
  
}
