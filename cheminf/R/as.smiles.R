as.smiles <- function(...)
  UseMethod("as.smiles")

as.smiles.sdfs <- function(sdfs, ncore=NULL, ...) {
  ## Parallelized by parallel package
  ncore <- bio3d::setup.ncore(ncore, bigmem = FALSE)
  
  if(ncore>1)
    mylapply <- parallel::mclapply
  else
    mylapply <- lapply

  ## convert to smiles
  all.smiles <- unlist(mylapply(sdfs, as.smiles.sdf))
  smi <- data.frame(matrix(all.smiles, ncol=2, byrow=TRUE), stringsAsFactors=FALSE)
  colnames(smi) <- c("smiles", "id")
  
  return(smi)
}
  
## convert a sdf object to smiles
as.smiles.sdf <- function(sdf, ...) {
  if(is.sdf(sdf)) {
    tmpf <- tempfile()
    outfile <- tempfile()
    write.sdf(sdf, file=tmpf)
  }
  else {
      if(is.null(sdf) | is.na(sdf)) {
          return(rep(NA, NA))
      }
      else {
          stop("provide a sdf object")
      }
  }

  ## Check if the program is executable
  .testBabel()
  
  cmd <- paste("-isdf", tmpf, "-ocan", outfile, sep=" ")
  success <- .runBabel(cmd, ...)

  if(success==0) {
    smiles <- read.smiles(outfile)
    unlink(c(tmpf, outfile))
    return(smiles)
  }
  else {
    stop("babel problem?")
  }
}

as.smiles.default <- function(x, id = NULL, ...) {
  x <- as.vector(x)
  if(is.null(id))
    id <- seq(1, length(x))
  if(length(x)!=length(id))
    stop("unequal lengths of x and id")
  
  smiles <- data.frame("smiles"=x, "id"=id, stringsAsFactors=FALSE)
  class(smiles) <- c("data.frame", "smiles")
  return(smiles)
}

##babel does not read inchikey format
#as.smiles.inchi <- function(x, ...) {
 
  ## Check if the program is executable
#  .testBabel()
  
#  tmpf <- tempfile()
#  outfile <- tempfile()
#  writeLines(x, con=tmpf, sep="\n")
  
#  cmd <- paste("-iinchikey", tmpf, "-ocan", outfile, sep=" ")
#  success <- .runBabel(cmd, ...)
  
#  if(success==0) {
#    smiles <- read.smiles(outfile)
#    unlink(c(tmpf, outfile))
#    return(smiles)
#  }
#  else {
#    stop("babel problem?")
#  }
#}

as.smiles.chembl <- function(comp, ...) {
  if(!is.compound(comp))
    stop("provide a chembl compound object as obtained from function 'chemb.compound'")
  
  smi <- as.smiles.default(comp$smiles, id=comp$chemblId)
  return(smi)
}
