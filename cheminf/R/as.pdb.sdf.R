#as.pdb <- function(...)
#  UseMethod("as.pdb")

as.pdb.sdf <- function(sdf, ...) {
  
  natoms <- nrow(sdf$atom)
  xyz <- sdf$xyz
  tmp.pdb <- list()
  
  
  tmp.pdb$atom <- data.frame(cbind(rep("ATOM", natoms),
                                   seq(1, natoms),
                                   sdf$atom[,"elety"],
                                   NA,
                                   rep("UNK", natoms),
                                   rep(" ", natoms),
                                   rep(1, natoms),
                                   NA,
                                   sdf$atom[,"x"], sdf$atom[,"y"], sdf$atom[,"z"],
                                   NA, NA, NA, NA, NA),
                             stringsAsFactors=FALSE)

  
  colnames(tmp.pdb$atom) <- c("type", "eleno", "elety", "alt", "resid",
                              "chain", "resno", "insert",
                              "x", "y", "z", "o", "b", "segid", "elesy", "charge")
  
  tmp.pdb$xyz <- xyz
  class(tmp.pdb) <- "pdb"
  ca.inds        <- rep(FALSE, natoms)
  return(tmp.pdb)
}
