orient.sdf <- function(sdf, verbose=FALSE) {

  pdb <- as.pdb.sdf(sdf)
  xyz <- bio3d::as.xyz(bio3d::orient.pdb(pdb, verbose=verbose))

  return(xyz)
}
