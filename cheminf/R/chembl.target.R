
## get target summary
chembl.target <- function(id, type='chemblid', verbose=TRUE) {
  oopsa <- requireNamespace("XML", quietly = TRUE)
  oopsb <- requireNamespace("RCurl", quietly = TRUE)
  if(!all(c(oopsa, oopsb)))
    stop("Please install the XML and RCurl package from CRAN")
  
  valid.types <- c('chemblid', 'uniprot')
  type <- pmatch(type, valid.types)
  if (is.na(type)) stop("Invalid type specified")
  url <- switch(type,
                url = 'https://www.ebi.ac.uk/chemblws/targets/',
                url = 'https://www.ebi.ac.uk/chemblws/targets/uniprot/')
  url <- sprintf('%s%s', url, id)

  if(verbose)
    cat("  Looking up URL:", url, "\n")
  
  h <- RCurl::getCurlHandle()
  d <- RCurl::getURL(url, curl=h)
  status <- RCurl::getCurlInfo(h)$response.code
  ctype <- RCurl::getCurlInfo(h)$content.type
  rm(h)
  if (status == 200) {
    xml <- XML::xmlParse(d)
    data <- XML::xmlToDataFrame(XML::getNodeSet(xml, "/target"), stringsAsFactors=FALSE)
    cat("  CHEMBL target entry succesfully read \n")
    return(data)
  } else {
    return(d)
  }
}



