\name{as.pdb.sdf}
\alias{as.pdb.sdf}
\title{ Convert SDF to PDB. }
\description{
  Convert a molecule in SDF format to PDB format.
}
\usage{
\method{as.pdb}{sdf}(sdf, \dots)
}
\arguments{
  \item{sdf}{ a SDF structure object obtained from
    \code{\link{read.sdf}}. }
  \item{\dots}{ arguments passed to and from functions. }
}
\details{
  This function provides basic utility to convert an \code{SDF} object
  to \code{PDB} object.
}
\value{
  Returns a \sQuote{PDB} object.
}
\references{
  Grant, B.J. et al. (2006) \emph{Bioinformatics} \bold{22}, 2695--2696.
}
\author{ Lars Skjaerven }
\seealso{
  \code{\link{read.sdf}},
}
\examples{
f   <- system.file("examples/aspirin-3d.sdf", package="cheminf")
sdf <- read.sdf(f)
pdb <- as.pdb.sdf(sdf)
}
\keyword{ pdb }
