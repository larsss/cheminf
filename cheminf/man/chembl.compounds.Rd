\name{chembl.compounds}
\alias{chembl.compounds}
\alias{print.compounds}
\title{
  Retrieve Compound Information from ChEMBL
}
\usage{
chembl.compounds(query, type='similarity', sim=90, order=TRUE, verbose=TRUE)
\method{print}{compounds}(x, \dots)
}
\arguments{
  \item{query}{ a character string of the ChEMBL ID, or an InChI key. }
  \item{type}{ \sQuote{chemblid} or \sQuote{stdinchi}. }
  \item{sim}{ numeric value of similarity cutoff. }
  \item{order}{ logica, if TRUE the compounds will be sorted based on
    similarity towards the query compound. }
  \item{verbose}{ logical, if TRUE some additional information is
    printed. }
  \item{x}{ a PDB structure object obtained from
    \code{\link{chembl.compounds}}.  }
  \item{\dots}{ additional arguments to \sQuote{print}. }
}
\description{
  Basic functionality to retrieve a compound entry from the ChEMBL database.
}
\author{
  Rajarshi Guha,
  Lars Skjaerven
}
\value{
  \code{chembl.compound} returns a list with the following named fields:
}
\seealso{
  \code{\link{chembl.compound}},
  \code{\link{chembl.target}}
}
\keyword{ chembl }
