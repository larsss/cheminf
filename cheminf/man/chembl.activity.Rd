\name{chembl.activity}
\alias{chembl.activity}
\title{
  Retrieve Activity Data from ChEMBL
}
\usage{
chembl.activity(chemblid, type='compound', verbose=TRUE)
}
\arguments{
  \item{chemblid}{ a character string of the ChEMBL ID. }
  \item{type}{ \sQuote{compound},  \sQuote{target}, or \sQuote{assay}. }
  \item{verbose}{ logical, if TRUE some additional information is printed. }
}
\description{
  Basic functionality to retrieve a compound entry from the ChEMBL database.
}
\author{
  Rajarshi Guha,
  Lars Skjaerven
}
\value{
  Returns a list with the following named fields:
}
\seealso{
  \code{\link{chembl.compound}},
  \code{\link{chembl.assay}}
}
\keyword{ chembl }
